package com.sherko.tableproject.resource;

import com.sherko.tableproject.model.UserRegistration;
import com.sherko.tableproject.repository.UserrRegRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@RequestMapping("/login")
@CrossOrigin("http://localhost:3000")
public class UserRegResource {
    @Autowired
    UserrRegRepository userrRegRepository;



    @GetMapping("/usersReg")
    public List<UserRegistration> getAll() {
        return userrRegRepository.findAll();
    }

    @GetMapping("/{name}")
    public int getUser(@PathVariable("name") final String nameReg) {
        return userrRegRepository.findByNameReg(nameReg).size();
        /// List<UserRegistration> arrayRegistration = listOptional.orElseThrow(() -> new RuntimeException("No users Founds"));
    }
    @GetMapping("/log/{nameL}/{passL}")
    public boolean getUserLog(@PathVariable("nameL") final String nameLog, @PathVariable("passL") final String passLog) {

//        int index = userrRegRepository.
        List<UserRegistration> registr = userrRegRepository.findByNameReg(nameLog);
//        return registr.get(0).getPasswordReg();
        if (registr.get(0).getPasswordReg().equals(passLog)){
            return true;
        }
        else return false;

    }


    @PostMapping(path = "/members", consumes = "application/json", produces = "application/json")
    public UserRegistration createProject(@RequestBody UserRegistration users) {
        return userrRegRepository.save(users);
    }

//    @PostMapping(path = "/users/all")
//    public String createProject(@RequestBody UserRegistration[] users) {
//        userrRegRepository.deleteAll();
//        for (UserRegistration u : users) {
//            userrRegRepository.save(u);
//        }
//        return "pe";
//    }


}
