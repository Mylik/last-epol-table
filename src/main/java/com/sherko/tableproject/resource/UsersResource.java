package com.sherko.tableproject.resource;


import java.util.List;
import java.util.Optional;

import com.sherko.tableproject.model.Users;
import com.sherko.tableproject.repository.UsersRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;


@RestController
@RequestMapping("/rest")
@CrossOrigin("http://localhost:3000")
public class UsersResource {

    @Autowired
    UsersRepository usersRepository;

    @GetMapping("/users")
    public List<Users> getAll() {
        return usersRepository.findAll();
    }

    @GetMapping("/{name}")
    public List<Users> getUser(@PathVariable("name") final String name) {
        Optional<List<Users>> listOptional = usersRepository.findByName(name);

        List<Users> users = listOptional.orElseThrow(() -> new RuntimeException("No users Founds"));
        //.orElse(new ArrayList<>());
        return users;
    }

    @GetMapping("/id/{id}")
    public Users getId(@PathVariable("id") final Integer id) {
        return usersRepository.findOne(id);
    }

    @GetMapping("/update/{id}/{name}")
    public Users update(@PathVariable("Id") final Integer id, @PathVariable("name") final String name) {

        Users users = getId(id);
        users.setName(name);

        return usersRepository.save(users);
    }

    // Create a new Project
    //@PostMapping(path = "/users")
    @PostMapping(path = "/members", consumes = "application/json", produces = "application/json")
    public Users createProject(@RequestBody Users users) {
        return usersRepository.save(users);
    }

    @PostMapping(path = "/users/all")
    public String createProject(@RequestBody Users[] users) {
        usersRepository.deleteAll();
        for (Users u : users) {
            usersRepository.save(u);
        }
        return "pe";
    }
}