package com.sherko.tableproject.resource;

import com.sherko.tableproject.model.UserImage;
import com.sherko.tableproject.model.Users;
import com.sherko.tableproject.repository.UserRepImage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/image")
@CrossOrigin("http://localhost:3000")
public class UserResImage {

    @Autowired
    UserRepImage userRepImage;

    @GetMapping("/all")
    public List<UserImage> getAll() {
        return userRepImage.findAll();
    }


    @PostMapping(path = "/members", consumes = "application/json", produces = "application/json")
    public UserImage createProject(@RequestBody UserImage users) {
        return userRepImage.save(users);
    }

}
