package com.sherko.tableproject.resource;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

@Controller
@CrossOrigin("http://localhost:3000")
public class FileUploadController {

    @RequestMapping(value="/upload", method=RequestMethod.GET)
    public @ResponseBody String provideUploadInfo() {
        return "Вы можете загружать файл с использованием того же URL.";
    }

    @RequestMapping(value="/upload/{name}", method=RequestMethod.POST)
    public @ResponseBody String handleFileUpload(@PathVariable("name") String name, @RequestParam("file") MultipartFile file){
//        name = "rrrrr";
        System.out.print("File ---"+file);
        if (!file.isEmpty()) {
            try {
                byte[] bytes = file.getBytes();
                BufferedOutputStream stream =
                        new BufferedOutputStream(new FileOutputStream(new File("src/frontend/src/image/" + name + ".png")));
                stream.write(bytes);
                stream.close();
                return "Вы удачно загрузили " + name + " в " + "src/frontend/src/image/";
            } catch (Exception e) {
                return "Вам не удалось загрузить " + name + " => " + e.getMessage();
            }
        } else {
            return "Вам не удалось загрузить " + name + " потому что файл пустой.";
        }
    }
//
}