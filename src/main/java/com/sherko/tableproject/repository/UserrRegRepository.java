package com.sherko.tableproject.repository;

import com.sherko.tableproject.model.UserRegistration;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface UserrRegRepository extends JpaRepository<UserRegistration, Integer>{

  List<UserRegistration> findByNameReg(String nameReg);

}
