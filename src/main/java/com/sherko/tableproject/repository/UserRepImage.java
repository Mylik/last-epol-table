package com.sherko.tableproject.repository;

import com.sherko.tableproject.model.UserImage;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface UserRepImage extends JpaRepository<UserImage, Integer>{
//     List<UserImage> findByNameReg(String nameReg);
}