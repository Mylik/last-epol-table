package com.sherko.tableproject.model;

import javax.persistence.*;

@Entity
@Table(name = "image1", catalog = "table1")
public class UserImage {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")

    private Long idImg;
    private String nameImg;


    public Long getIdImg() {
        return idImg;
    }
    public void setIdImg(Long idImg) {
        this.idImg = idImg;
    }
    public String getNameImg() {
        return nameImg;
    }
    public void setNameImg(String nameImg) {
        this.nameImg = nameImg;
    }
}
