import React from 'react'
import UserTable from './components/UserTable'
import BurgenMenu from './components/BurgenMenu'
import axios from 'axios'
import a from './logo.svg'
// import Dropdown from 'react-dropdown'
import 'react-dropdown/style.css'

import '@trendmicro/react-buttons/dist/react-buttons.css';
import Dropdown, {
    DropdownToggle,
    DropdownMenu,
    DropdownMenuWrapper,
    MenuItem,
    DropdownButton
} from '@trendmicro/react-dropdown';

// Be sure to include styles at some point, probably during your bootstraping
import '@trendmicro/react-buttons/dist/react-buttons.css';
import '@trendmicro/react-dropdown/dist/react-dropdown.css';


import './App.css'
import data from './db.json'


class App extends React.Component {
    ;

    constructor(props) {
        super(props)
        this.state = {
            data: [], image: [], searchData: [], direction: {name: 'asc', age: 'asc',},
            login: false,
            userLogin: false
        }

        this.sortBy = this.sortBy.bind(this);
        this.deleteBlock = this.deleteBlock.bind(this);
        this.update = this.update.bind(this);
        this.submit = this.submit.bind(this);
        this.changeList = this.changeList.bind(this);
        this.reset = this.reset.bind(this);

        this.login = this.login.bind(this);
        this.userLogin = this.userLogin.bind(this);
    }

    // componentDidMount() {
    //     axios.get('http://localhost:9096/image/all/')
    //         .then(response => response.image)
    //         .then(users => this.setState({image: users}))
    //         .catch(error => console.error(error));
    // }


    componentDidMount() {

        if (localStorage.getItem('login') === 'yes') this.setState({login: true});
        // if (localStorage.getItem(userLogin === true) this.setState({userLogin: true});

        axios.get('http://localhost:9096/rest/users/')
            .then(response => response.data)
            .then(users => this.setState({data: users}))
            .catch(error => console.error(error));

        axios.get('http://localhost:9096/image/all/')
            .then(response => response.data)
            .then(users => this.setState({image: users}))
            .catch(error => console.error(error));
    }

    login(yesorno) {
        this.setState({login: yesorno});
        yesorno ? localStorage.setItem('login', 'yes') :
            localStorage.setItem('login', 'no');
    }

    userLogin(yesorno) {
        this.setState({userLogin: yesorno});
        yesorno ? localStorage.setItem('userLogin', 'yes') :
            localStorage.setItem('userLogin', 'no');
    }

    reset() {
        console.log(this.state.searchData)
        if (this.state.searchData.length) {
            this.setState({data: this.state.searchData});
            // this.state.searchData = new Array(); // add setState
            this.setState({searchData: []})
        }
    }

    deleteBlock(index) {
        var arr = this.state.data;
        arr.splice(index, 1);
        this.setState({data: arr});
    }


    update(index, data) {
        const arr = this.state.data;
        arr[index] = data;
        this.setState({data: arr});
        alert(data.image);

    }

    submit() {
        console.log(this.state.data);
        axios.post('http://localhost:9096/rest/users/all', this.state.data)
            .then(response => response.data)
            .then(inf => {
                console.log(inf)
            })
            .catch(error => console.error(error));
    }

    sortBy(key) {
        let dir = this.state.direction;

        if (dir[key] === 'asc') dir[key] = 'desc';
        else dir[key] = 'asc';

        this.setState({
            data: this.state.data.sort((a, b) => {
                return this.state.direction[key] === 'asc'
                    ? (a[key]) < (b[key])
                    : (b[key]) < (a[key])
            }),
            direction: dir
        })
    }

    changeList(prop) {
        if (!this.state.searchData.length) {
            this.setState({searchData: this.state.data})
            console.log("ss");
        }

        let term = prop.toLowerCase(),
            users = this.state.data,
            newList = [];

        for (var i = 0; i < users.length; i++) {
            if (users[i]['name'].toLowerCase().indexOf(term) > -1) {
                //push data into results array
                newList.push(users[i]);
            }
        }
        this.setState({data: newList});
    }


    save() {
        const data = {id: this.props.index};
        data.name = this.refs.name.value;
        data.age = +this.refs.age.value;
        data.lname = this.refs.lname.value;
        data.phone = this.refs.phone.value;
        data.image = this.refs.image.value;
        if (data.name && data.age) {
            this.setState({edit: false});
            this.props.delete(this.props.countIndex)
            this.props.update(this.props.index, data);
        }

    }

    render() {
        const options = [
            'one', 'two', 'three'
        ];
        return (
            <div className="page-container">
                {/*<img src={a} />*/}
                <BurgenMenu
                    login={this.login}/>

                <div className="search">

                    <input className="form-control" type="text" placeholder="Search by name..."
                           onChange={event => this.changeList(event.target.value)}/>

                    <button className="btn btn-danger" onClick={this.reset}><i className="fas fa-ban"></i> reset
                    </button>

                </div>
                <UserTable
                    data={this.state.data}
                    sortBy={this.sortBy}
                    deleteBlock={this.deleteBlock}
                    update={this.update}
                    submit={this.submit}
                    direction={this.state.direction}
                    login={this.state.login}
                />

                {/*{console.log("test", this.state.image)}*/}
                
                {/*{*/}
                    {/*(!!this.state.image) ?*/}
                        {/*this.state.image.map((img, index) => (*/}
                            {/*<div>*/}
                                {/*/!*<p>{img.idImg}</p>*!/*/}
                                {/*<p># {img.idImg} name {img.nameImg}</p>*/}
                                {/*/!*<img src={require(`image/${img.nameImg}.png`)} width="50px" />*!/*/}
                                {/*<img src={require(`./image/${img.nameImg}.png`)} width="100px" alt="Red dot"/>*/}
                            {/*</div>*/}
                        {/*)) : console.log("123")*/}
                {/*}*/}

                {/*<Dropdown>*/}
                    {/*<Dropdown.Toggle title="Select an option"/>*/}
                    {/*<Dropdown.Menu>*/}
                        {/*/!*<MenuItem>*!/*/}
                            {/*/!*Menu item one*!/*/}
                        {/*/!*</MenuItem>*!/*/}
                        {/*/!*<MenuItem>*!/*/}
                            {/*/!*Menu item two*!/*/}
                        {/*/!*</MenuItem>*!/*/}
                        {/*/!*<MenuItem>*!/*/}
                            {/*/!*Menu item three*!/*/}
                        {/*/!*</MenuItem>*!/*/}
                        {/*{this.state.image.map((img, index) => (*/}
                        {/*<MenuItem>*/}
                            {/*/!*<p>{img.idImg}</p>*!/*/}
                            {/*<p># {img.idImg} name {img.nameImg}</p>*/}
                            {/*/!*<img src={require(`image/${img.nameImg}.png`)} width="50px" />*!/*/}
                            {/*<img src={require(`./image/${img.nameImg}.png`)} width="100px" alt="Red dot"/>*/}
                        {/*</MenuItem>))}*/}

                        {/*//перенести с добавление, убрать снизу вывод , найти скролинг, обработка нажатия*/}

                    {/*</Dropdown.Menu>*/}
                {/*</Dropdown>*/}

            </div>
        )
    }
}

export default App
