import React from 'react'
import axios, { post } from 'axios';

class SimpleReactFileUpload extends React.Component {

  constructor(props) {
    super(props);
    this.state ={
        file:null,
        name: ""
    }
    this.onFormSubmit = this.onFormSubmit.bind(this)
    this.onChange = this.onChange.bind(this)
    this.onChange1 = this.onChange1.bind(this)
    this.fileUpload = this.fileUpload.bind(this)
  }
  
  
  onFormSubmit(e){
    // e.preventDefault() // Stop form submit
    this.fileUpload(this.state.name, this.state.file).then((response)=>{
      console.log(response.data);
    })

      const formData = {
          idImg: 1111,
          nameImg: this.state.name
      }

      post(`http://localhost:9096/image/members`, formData)
          .then(response => response.data)
          .then(inf => {
              console.log(inf)
          })
          .catch(error => console.error(error));


  }

  onChange(e) {
      console.log(e.target.files[0]);
    this.setState({file:e.target.files[0]})
  }
    onChange1(e) {
    // console.log(e.target.nameImg);
    console.log(e.target.value);
        this.setState({name:e.target.value})
    }

  fileUpload(name,file){
    const url = 'http://localhost:9096/upload/'+name;
    const formData = new FormData();

    formData.append('file',file);
    const config = {
        headers: {
            'content-type': 'multipart/form-data'
        }
    }
    return  post(url, formData,config)
  }

  render() {
    return (
      <form onSubmit={this.onFormSubmit}>
        <p><input type="file" onChange={this.onChange} /></p>
        <p><input type="text" name="nameImg" placeholder="Имя картинки"  onChange={this.onChange1} /></p>
        <p><button type="submit">Upload</button></p>
      </form>
   )
  }
}



export default SimpleReactFileUpload
