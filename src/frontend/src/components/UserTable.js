import React from 'react'
import RowTable from './RowTable'
import AddRow from './AddRow'
import '../App.css'
import axios from 'axios'

class UserTable extends React.Component {
    constructor(props) {
        super(props);
        this.state = {image:[], add: false, item: 0, count: []}

        this.add = this.add.bind(this)
        this.delete = this.delete.bind(this)

        const image = this.props.row1;
    }

    componentDidMount() {


    }
    
    add() {
        const temp = this.state.count;
        let t = this.state.item;
        temp.push(t++);
        this.setState({
            //add: true,
            count: temp
        });
    }

    delete(index) {
        const arr = this.state.count;
        arr.splice(index, 1);
        this.setState({count: arr});
    }
    
    render() {
        return (
            <content>
                <table>
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>
                            <a onClick={() => this.props.sortBy('name')}>
                                {/*Сортировка по имени*/
                                    (this.props.direction.name === 'asc') ? "First Name↑" : "First Name↓"
                                }

                            </a>
                        </th>
                        <th>Last Name</th>
                        <th>
                            <a onClick={() => this.props.sortBy('age')}>
                                {/*Сортировка по возрасту*/
                                    (this.props.direction.age === 'asc') ? "Age↑" : "Age↓"
                                }
                            </a>
                        </th>
                        <th>Phone</th>
                        <th>Image</th>
                        <th>Property</th>
                    </tr>
                    </thead>
                    <tbody>
                    {
                        this.props.data.map((row, index) => (

                            // deleteBlock={this.deleteBlock}
                            <RowTable
                                key={index}
                                row={row}
                                index={index}
                                deleteBlock={this.props.deleteBlock}
                                update={this.props.update}/>

                        ))
                    }
                    {

                        this.state.count.map((row, index) => (
                            <AddRow key={this.props.data.length + index}
                                    index={this.props.data.length + index}
                                    countIndex={index}
                                    delete={this.delete}
                                    update={this.props.update}/>
                        ))
                    }

                    {
                        (this.props.login) ? <tr className="trbtn">
                            <td style={{padding: 0}} colSpan="8">
                                <button className="btn blue mt14" onClick={() => this.props.submit()}> Submit</button>
                            </td>
                            <td>
                                <button className="btn green mt15" onClick={this.add}> Add</button>
                            </td>
                        </tr> : ''
                    }
                    

                    </tbody>
                </table>

            </content>
        )
    }

}

export default UserTable