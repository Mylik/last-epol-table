import React from 'react'
import axios from 'axios'
import 'react-dropdown/style.css'

import '@trendmicro/react-buttons/dist/react-buttons.css';
import Dropdown, {
    DropdownToggle,
    DropdownMenu,
    DropdownMenuWrapper,
    MenuItem,
    DropdownButton
} from '@trendmicro/react-dropdown';
import '@trendmicro/react-buttons/dist/react-buttons.css';
import '@trendmicro/react-dropdown/dist/react-dropdown.css';


class AddRow extends React.Component {
    constructor(props) {
        super(props);
        this.state = {image: [], edit: true, phoneErr: false, ageErr: false, nameErr: false, lnameErr: false, selected: ""}
        this.save = this.save.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.handleChangePh = this.handleChangePh.bind(this);
        this.handleChangeN = this.handleChangeN.bind(this);
        this.handleChangeLn = this.handleChangeLn.bind(this);

    }

    componentDidMount() {
        axios.get('http://localhost:9096/image/all/')
            .then(response => response.data)
            .then(users => this.setState({image: users}))
            .catch(error => console.error(error));
    }

    /*Сохранение нового пользователя*/
    save() {

        const data = {id: this.props.index};
        //data.name = this.refs.name.value;
        // data.age = this.refs.age.value;
        //data.lname = this.refs.lname.value;
        // data.phone = this.refs.phone.value;
         data.image = this.state.selected;

         console.log("Selected --------------------------------------", this.state.selected);

        let nameSav = RegExp(/^[a-zA-Zа-яёА-ЯЁ\s\-]+$/);
        let teleph =  RegExp(/^(\s*)?(\+)?([- _():=+]?\d[- _():=+]?){10,14}(\s*)?$/);

        if(nameSav.test(this.refs.name.value))
            data.name = this.refs.name.value;
        else
            this.setState({nameErr: true})

        if(nameSav.test(this.refs.lname.value))
            data.lname = this.refs.lname.value;
        else
            this.setState({lnameErr: true})


        if (teleph.test(this.refs.phone.value))
            data.phone = this.refs.phone.value;
        else
            this.setState({phoneErr: true});

        if(this.refs.age.value < 100 && this.refs.age.value > 6)
            data.age = this.refs.age.value;

        else
            this.setState({ageErr: true});


        const toDataURL = url => fetch(url)
            .then(response => response.blob())
            .then(blob => new Promise((resolve, reject) => {
                const reader = new FileReader()
                reader.onloadend = () => resolve(reader.result)
                reader.onerror = reject
                reader.readAsDataURL(blob)
            }))


        // toDataURL(data.image)
        //     .then(dataUrl => {
        //         console.log('RESULT:', dataUrl.split(",")[1]);
        //         data.image = dataUrl.split(",")[1];
        //         console.log(data.image);
        //
        //     })

        console.log(data.image);

        // data.image = this.refs.image.value;
        if (data.name && data.age) {
            this.setState({edit: false});
            this.props.delete(this.props.countIndex)
            this.props.update(this.props.index, data);
        }
        //alert(data.image)

    }

    handleChange(event) {
        if(!event.target.value)
            this.setState({ageErr: false});
    }
    handleChangePh(event) {
        if(!event.target.value)
            this.setState({phoneErr: false});
    }
    handleChangeN(event) {
        if(!event.target.value)
            this.setState({nameErr: false});
    }
    handleChangeLn(event) {
        if(!event.target.value)
            this.setState({lnameErr: false});
    }

    // addImage(){
    //     data.image = this.img.name;
    // }

    /*Поле при добавлении нового пользователя*/
    render() {
        const { selected } = this.state;
        if (this.state.edit)
            return (<tr>
                <td>{this.props.index}</td>
                {(this.state.nameErr) ? <td><input className="error" ref="name" type="text" onChange={this.handleChangeN} /></td>
                    : <td><input ref="name" type="text" placeholder="Имя"/></td>}

                {(this.state.lnameErr) ? <td><input className="error" ref="lname" type="text" onChange={this.handleChangeLn}/></td>
                    : <td><input ref="lname" type="text" placeholder="Фамилия"/></td>}

                {(this.state.ageErr) ? <td><input className="error" ref="age" type="text" required onChange={this.handleChange}/></td>
                    : <td><input ref="age" type="text" placeholder="Возраст" /></td>}

                {(this.state.phoneErr) ? <td><input className="error" ref="phone" type="text" required onChange={this.handleChangePh}/></td>
                    : <td><input ref="phone" type="text" placeholder="Номер"/></td>}

                <td>
                    {/*<input ref="image" type="text" placeholder="URL-адрес"/>*/}


                    <Dropdown
                        onSelect={(eventKey, event) => {
                            this.setState({ selected: eventKey });
                        }}
                    >
                        <Dropdown.Toggle title="Select an option"/>
                        <Dropdown.Menu
                            style={{
                                display: 'block',
                                minWidth: 240,
                                maxHeight: 150,
                                overflowY: 'auto'
                            }}
                        >
                            {/*<MenuItem>*/}
                            {/*Menu item one*/}
                            {/*</MenuItem>*/}
                            {/*<MenuItem>*/}
                            {/*Menu item two*/}
                            {/*</MenuItem>*/}
                            {/*<MenuItem>*/}
                            {/*Menu item three*/}
                            {/*</MenuItem>*/}
                            {this.state.image.map((img, index) => (
                                <MenuItem eventKey={img.nameImg}>
                                    {/*<p>{img.idImg}</p>*/}
                                    <p># {img.idImg} name {img.nameImg}</p>
                                    {/*<img src={require(`image/${img.nameImg}.png`)} width="50px" />*/}
                                    <img src={require(`../image/${img.nameImg}.png`)} width="100px" alt="Red dot" />
                                </MenuItem>))}

                            //перенести с добавление, убрать снизу вывод , найти скролинг, обработка нажатия

                        </Dropdown.Menu>
                    </Dropdown>
                </td>
                <td>
                    <div className="btnCont">
                        <a onClick={this.save} className="btn green ">Add</a>
                        <a onClick={() => this.props.delete(this.props.countIndex)} className="btn blue ">Delete</a>
                    </div>
                </td>
            </tr>)
        else return;
    }
}

export default AddRow
