import React from 'react'
import a from '../logo.svg'


class RowTable extends React.Component {
    constructor(props) {
        super(props);
        this.state = {edit: false}

        this.edit = this.edit.bind(this)
        this.save = this.save.bind(this)
    }





    edit() {
        this.setState({edit: true});
    }

    /*Сохранение */
    save() {
        this.setState({edit: false});
        const data = this.props.row;
        //const image = this.props.row1;
        data.name = this.refs.name.value;
        data.lname = this.refs.lname.value;
        data.age = this.refs.age.value;
        data.phone = this.refs.phone.value;
        data.image = this.refs.image.value;
        this.props.update(this.props.index, data);

    }


    render() {
        if (!this.state.edit) {
            return (
                <tr>
                    <td>{this.props.index}</td>
                    <td>{this.props.row.name}</td>
                    <td>{this.props.row.lname}</td>
                    <td>{this.props.row.age}</td>
                    <td>{this.props.row.phone}</td>
                    <td>
                        {/*{(!this.props.row.image)?<img src={a} width="50px" alt="Red dot"/> : <img src={`${this.props.row.image}`} width="50px" alt="Red dot"/>}*/}
                        <img src={require(`../image/${this.props.row.image}.png`)} width="50px" alt="Red dot"/>
                    </td>
                    <td>
                        <div className="btnCont">
                            <a className="btn green " onClick={this.edit}>Edit</a>
                            <a className="btn blue" onClick={() => this.props.deleteBlock(this.props.index)}>Delete</a>
                        </div>

                    </td>
                </tr>)
        } else {
            return (<tr>
                <td>{this.props.index}</td>
                <td><input ref="name" type="text" placeholder="Имя" defaultValue={this.props.row.name}/></td>
                <td><input ref="lname" type="text" placeholder="Фамилия" defaultValue={this.props.row.lname}/></td>
                <td><input ref="age" type="text" placeholder="Возраст" defaultValue={this.props.row.age}/></td>
                <td><input ref="phone" type="text" placeholder="Телефон" defaultValue={this.props.row.phone}/></td>
                <td><input ref="image" type="text" placeholder="Имя изображения" defaultValue={this.props.row.image}/>

                </td>
                <td>
                    <a onClick={this.save} className="btn green">OK</a>
                </td>
            </tr>)
        }
    }

}

export default RowTable
